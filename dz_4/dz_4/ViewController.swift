//
//  ViewController.swift
//  dz_4
//
//  Created by Andrey Bakanov on 3/17/19.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit
// (void)setupViewController{
//    self.view1.layer.cornerRadius = 6.0;
//}

class ViewController: UIViewController {
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        drawBox4()
       

        
    }
    // treugol
    func drawBox1() {
        let box = UIView.init(frame: CGRect(x: 110, y: 230, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        let box1 = UIView.init(frame: CGRect(x: 150, y: 230, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        let box2 = UIView.init(frame: CGRect(x: 190, y: 230, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        let box3 = UIView.init(frame: CGRect(x: 110, y: 190, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        let box4 = UIView.init(frame: CGRect(x: 150, y: 190, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        let box5 = UIView.init(frame: CGRect(x: 110, y: 150, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        
        view.addSubview(box)
        box1.backgroundColor = UIColor.blue
        view.addSubview(box1)
        box2.backgroundColor = UIColor.blue
        view.addSubview(box2)
        
        box3.backgroundColor = UIColor.blue
        view.addSubview(box3)
        box4.backgroundColor = UIColor.blue
        view.addSubview(box4)
        
        box5.backgroundColor = UIColor.blue
        view.addSubview(box5)
        
        
        
    }
        // piramida
    func drawBox2() {
        let box = UIView.init(frame: CGRect(x: 110, y: 230, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        let box1 = UIView.init(frame: CGRect(x: 150, y: 230, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        let box2 = UIView.init(frame: CGRect(x: 190, y: 230, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        let box3 = UIView.init(frame: CGRect(x: 130, y: 190, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        let box4 = UIView.init(frame: CGRect(x: 170, y: 190, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        let box5 = UIView.init(frame: CGRect(x: 150, y: 150, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        
        view.addSubview(box)
        box1.backgroundColor = UIColor.blue
        view.addSubview(box1)
        box2.backgroundColor = UIColor.blue
        view.addSubview(box2)
        
        box3.backgroundColor = UIColor.blue
        view.addSubview(box3)
        box4.backgroundColor = UIColor.blue
        view.addSubview(box4)
        
        box5.backgroundColor = UIColor.blue
        view.addSubview(box5)
        
    }
    
    ////////////////////////////////////
    //kvadrat v kvadrate  i skruglenie
    func drawBox3() {
        let box = UIView.init(frame: CGRect(x: 150, y: 240, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        box.layer.borderWidth = 1
        box.layer.cornerRadius = box.bounds.width / 2
        
        let box1 = UIView.init(frame: CGRect(x: 140, y: 230, width: 50, height: 50))
        box1.backgroundColor = UIColor.red
        box1.layer.borderWidth = 1
        box1.layer.cornerRadius = box1.bounds.width / 2
        
        let box2 = UIView.init(frame: CGRect(x: 125, y: 215, width: 80, height: 80))
        box2.backgroundColor = UIColor.blue
        box2.layer.borderWidth = 1
        box2.layer.cornerRadius = box2.bounds.width / 2
        
        view.addSubview(box2)
        view.addSubview(box1)
        view.addSubview(box)
    }
    
    //kvadrat v kvadrate
    func drawBox4() {
        let box = UIView.init(frame: CGRect(x: 150, y: 240, width: 30, height: 30))
        box.backgroundColor = UIColor.blue
        
        let box1 = UIView.init(frame: CGRect(x: 140, y: 230, width: 50, height: 50))
        box1.backgroundColor = UIColor.red
        
        let box2 = UIView.init(frame: CGRect(x: 125, y: 215, width: 80, height: 80))
        box2.backgroundColor = UIColor.blue
        
        view.addSubview(box2)
        view.addSubview(box1)
        view.addSubview(box)
    }
   
    
    
}

